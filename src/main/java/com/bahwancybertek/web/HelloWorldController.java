package com.bahwancybertek.web;

import java.util.ArrayList;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

import com.bahwancybertek.domain.Campaign;
import com.bahwancybertek.domain.Customer;
import com.bahwancybertek.domain.TemplateValue;
import com.bahwancybertek.domain.example.Quote;
import com.bahwancybertek.service.HelloWorldService;

@Controller
public class HelloWorldController {
	
	private static Log logger = LogFactory.getLog(HelloWorldController.class);

	@Autowired
	private HelloWorldService helloWorldService;
	
	@Autowired
	private RestTemplate restTemplate;
	
	@Value("${airis.backend.endpoint}")
	private String airisBackendEndpoint;

	@GetMapping("/")
	@ResponseBody
	public String helloWorld() {
		return this.helloWorldService.getHelloMessage();
	}
	
	@GetMapping("/run")
	@ResponseBody
	public String run() {
		return this.helloWorldService.runPipeline();
	}
	
	@GetMapping("/quote")
	@ResponseBody
	public Quote quote() {
		Quote quote = restTemplate.getForObject("http://gturnquist-quoters.cfapps.io/api/random", Quote.class);
		logger.info(quote.toString());
		return quote;
	}
	
	@RequestMapping(value="/campaign", method = RequestMethod.GET)
	@ResponseBody
	public String campaign() {
		String resourceUrl = this.airisBackendEndpoint + "/campaigns/456789/run";

		Campaign campaign = fakeCampaign();
		
		HttpEntity<Campaign> request = new HttpEntity<Campaign>(campaign);
		ResponseEntity<Campaign> response = restTemplate.exchange(resourceUrl, HttpMethod.POST, request, Campaign.class);
		
		logger.info(response.toString());
		
		return response.toString();
	}
	
	private Campaign fakeCampaign() {
		
		ArrayList<TemplateValue> tvList = new ArrayList<TemplateValue>();
		tvList.add(new TemplateValue("ean123", "w123", "The good book #1"));
		tvList.add(new TemplateValue("ean122", "w122", "The good book #2"));
		tvList.add(new TemplateValue("ean121", "w121", "The good book #3"));
		
		ArrayList<Customer> customers = new ArrayList<Customer>();
		customers.add(new Customer("Billy", "Truong", "javabilly@gmail.com", "16266960069", tvList, null));
		customers.add(new Customer("Jojojo", "Dada", "jojojo@gmail.com", "16266960069", tvList, null));
		
		Campaign cam = new Campaign("58519d14ddc3504b1146de83", "b123", customers);
	
		return cam;
	}
	

}