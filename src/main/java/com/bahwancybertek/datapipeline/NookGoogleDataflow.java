package com.bahwancybertek.datapipeline;

import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.bahwancybertek.scheduler.ScheduledCampaigns;
import com.google.api.services.bigquery.model.TableRow;
import com.google.cloud.dataflow.sdk.Pipeline;
import com.google.cloud.dataflow.sdk.io.BigQueryIO;
import com.google.cloud.dataflow.sdk.io.Sink;
import com.google.cloud.dataflow.sdk.io.TextIO;
import com.google.cloud.dataflow.sdk.io.Sink.WriteOperation;
import com.google.cloud.dataflow.sdk.options.DataflowPipelineOptions;
import com.google.cloud.dataflow.sdk.options.Description;
import com.google.cloud.dataflow.sdk.options.PipelineOptions;
import com.google.cloud.dataflow.sdk.options.PipelineOptionsFactory;
import com.google.cloud.dataflow.sdk.options.Validation;
import com.google.cloud.dataflow.sdk.runners.BlockingDataflowPipelineRunner;
import com.google.cloud.dataflow.sdk.transforms.DoFn;
import com.google.cloud.dataflow.sdk.transforms.PTransform;
import com.google.cloud.dataflow.sdk.transforms.ParDo;
import com.google.cloud.dataflow.sdk.transforms.DoFn.ProcessContext;
import com.google.cloud.dataflow.sdk.values.PCollection;

/**
 * <p>
 * To run this starter example using managed resource in Google Cloud Platform,
 * you should specify the following command-line options:
 * --project=<YOUR_PROJECT_ID>
 * --stagingLocation=<STAGING_LOCATION_IN_CLOUD_STORAGE>
 * --runner=BlockingDataflowPipelineRunner
 */
public class NookGoogleDataflow {

	private static final String DEFAULT_TABLE = "analytics-platform-sandbox:nmprod.lkup_department";

	private static Log logger = LogFactory.getLog(NookGoogleDataflow.class);

	private static interface Options extends PipelineOptions {
		@Description("Table to read from, specified as <project_id>:<dataset_id>.<table_id>")
		@Validation.Required
		String getInput();

		void setInput(String value);

		@Description("Table to write to, specified as <project_id>:<dataset_id>.<table_id>. The dataset_id must already exist")
		@Validation.Required
		String getOutput();

		void setOutput(String value);
	}

	static class FormatTableRowAsTextFn extends PTransform<PCollection<TableRow>, PCollection<String>> {
		@Override
		public PCollection<String> apply(PCollection<TableRow> rows) {

			PCollection<String> results = rows.apply(ParDo.of(new DoFn<TableRow, String>() {
				@Override
				public void processElement(ProcessContext c) {
					TableRow row = c.element();
					Set<String> fields = row.keySet();
					int lastIndex = fields.size() -1;
					int currIndex = 0;

					StringBuilder sb = new StringBuilder();
					for (String field : fields) {
						sb.append(row.get(field));
						if (currIndex < lastIndex) {
							sb.append(",");
						}
						currIndex += 1;
					}
					c.output(sb.toString());
				}
			}));

			return results;
		}
	}

	// .apply(
	// BigQueryIO.Write.to(outputTable)
	// .withSchema(schema)
	// .withCreateDisposition(BigQueryIO.Write.CreateDisposition.CREATE_IF_NEEDED)
	// .withWriteDisposition(BigQueryIO.Write.WriteDisposition.WRITE_TRUNCATE)
	// )
	
	// Build the table schema for the output table.
	//	List<TableFieldSchema> fields = new ArrayList<>();
	//	fields.add(new TableFieldSchema().setName("department_code").setType("STRING"));
	//	fields.add(new TableFieldSchema().setName("department_description").setType("STRING"));
	//	TableSchema schema = new TableSchema().setFields(fields);
	public static void runPipeline(String[] args) {

		String projectId = "analytics-platform-sandbox";
//		String projectId = "analytics-nm-com-pg";
		String stagingLocation = "gs://apmc";
		String savingFilePath = stagingLocation + "/57fecd68ddc3507f8ab48e2f/v1/campaign-data.txt";

		DataflowPipelineOptions options = PipelineOptionsFactory.create().as(DataflowPipelineOptions.class);
		options.setRunner(BlockingDataflowPipelineRunner.class);
		options.setProject(projectId);
		options.setStagingLocation(stagingLocation);

		String inputTable = projectId + ":nmprod.lkup_department";
		String outputTable = projectId + ":test.apmc_test";
		
		String inputQuery = "SELECT ws.backend_customer_id as customer_id, ws.ean as ean "
				+ "FROM 		[" + projectId + ":nmprod.nook_wishlist] AS ws "
				+ "WHERE ws.item_last_updated_time >= DATE_ADD(CURRENT_TIMESTAMP(), -5, 'DAY')";
		
		
		Pipeline p = Pipeline.create(options);
		
		p.apply(BigQueryIO.Read.fromQuery(inputQuery))
		.apply(new FormatTableRowAsTextFn())
		.apply(TextIO.Write.named("ApmcDataAssembler").to(savingFilePath));

		p.run();
		

		
	}
	
	class NookFtp extends Sink {

		@Override
		public void validate(PipelineOptions options) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public WriteOperation createWriteOperation(PipelineOptions options) {
			// TODO Auto-generated method stub
			return null;
		}
		
	}
}