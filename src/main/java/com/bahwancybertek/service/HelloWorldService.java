package com.bahwancybertek.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.bahwancybertek.datapipeline.NookGoogleDataflow;

@Component
public class HelloWorldService {

	@Value("${app.name}")
	private String name;

	public String getHelloMessage() {
		return "Hello " + this.name;
	}
	
	public String runPipeline() {
		String[] args = new String[1];
		NookGoogleDataflow.runPipeline(args);
		
		return "NookGoogleDataflow.runPipeline just ran";
	}

}