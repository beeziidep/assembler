package com.bahwancybertek.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.bahwancybertek.domain.Campaign;
import com.bahwancybertek.domain.Customer;
import com.bahwancybertek.domain.TemplateValue;

@Component
public class CampaignService {
	
	private static Log logger = LogFactory.getLog(CampaignService.class);
	
	@Autowired
	private RestTemplate restTemplate;
	
	@Value("${airis.backend.endpoint}")
	private String airisBackendEndpoint;
	
	
	public List<Campaign> getDeliverableCampaigns() {
		String resourceUrl = this.airisBackendEndpoint + "/campaigns";
		String apiVersion = "api.v1";
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("app-x-v", apiVersion);
		
		HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
		
		logger.info(resourceUrl);
		logger.info(entity);
		
		ResponseEntity<List<Campaign>> response = restTemplate.exchange(resourceUrl, HttpMethod.GET, entity, new ParameterizedTypeReference<List<Campaign>>(){});
		
		logger.info(response);
		
		List<Campaign> campaigns = response.getBody();
		
		return campaigns;
	}
	
	
	private Campaign fakeCampaign() {
		
		ArrayList<TemplateValue> tvList = new ArrayList<TemplateValue>();
		tvList.add(new TemplateValue("ean123", "w123", "The good book #1"));
		tvList.add(new TemplateValue("ean122", "w122", "The good book #2"));
		tvList.add(new TemplateValue("ean121", "w121", "The good book #3"));
		
		ArrayList<Customer> customers = new ArrayList<Customer>();
		customers.add(new Customer("Billy", "Truong", "javabilly@gmail.com", "16266960069", tvList, null));
		customers.add(new Customer("Jojojo", "Dada", "jojojo@gmail.com", "16266960069", tvList, null));
		
		Campaign cam = new Campaign("58519d14ddc3504b1146de83", "b123", customers);
	
		return cam;
	}

}
