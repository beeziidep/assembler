package com.bahwancybertek.scheduler;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.bahwancybertek.domain.Campaign;
import com.bahwancybertek.service.CampaignService;

@Component
public class ScheduledCampaigns {

    private static Log logger = LogFactory.getLog(ScheduledCampaigns.class);

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
    
	@Autowired
	private CampaignService campaignService;
	

    @Scheduled(fixedRate = 60000)
    public void processDeliverableCampaigns() {
    	logger.info("The time is now :" + dateFormat.format(new Date()));
    	
    	List<Campaign> campaigns = campaignService.getDeliverableCampaigns();
    	
    	logger.info(campaigns);
 
    }
}