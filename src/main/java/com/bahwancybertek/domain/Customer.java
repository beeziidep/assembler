package com.bahwancybertek.domain;

import java.util.ArrayList;

public class Customer {
	private String firstName;
	private String lastName;
	private String email;
	private String mobilePhone;
	private ArrayList<String> deviceTokens;
	private ArrayList<TemplateValue> templateValues;
	
	public Customer(){
		super();
	}
	
	public Customer(String firstName, String lastName, String email, String mobilePhone, ArrayList<TemplateValue> templateValues, ArrayList<String> deviceTokens) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.mobilePhone = mobilePhone;
		this.templateValues = templateValues;
		this.deviceTokens = deviceTokens;
	}
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getMobilePhone() {
		return mobilePhone;
	}
	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}
	public ArrayList<String> getDeviceTokens() {
		return deviceTokens;
	}
	public void setDeviceTokens(ArrayList<String> deviceTokens) {
		this.deviceTokens = deviceTokens;
	}
	public ArrayList<TemplateValue> getTemplateValues() {
		return templateValues;
	}
	public void setTemplateValues(ArrayList<TemplateValue> templateValues) {
		this.templateValues = templateValues;
	}

	@Override
	public String toString() {
		return "Customer [firstName=" + firstName + ", lastName=" + lastName + ", email=" + email + ", mobilePhone="
				+ mobilePhone + ", deviceTokens=" + deviceTokens + ", templateValues=" + templateValues + "]";
	}
	
	
	
	
}
