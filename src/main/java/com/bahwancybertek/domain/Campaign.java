package com.bahwancybertek.domain;

import java.util.ArrayList;

public class Campaign {
	private String campaignId;
	private String batchId;
	private ArrayList<Customer> customers;
	
	public Campaign(){
		super();
	}
	public Campaign(String campaignId, String batchId, ArrayList<Customer> customers){
		super();
		this.campaignId = campaignId;
		this.batchId = batchId;
		this.customers = customers;
	}
	
	public String getCampaignId() {
		return campaignId;
	}
	public void setCampaignId(String campaignId) {
		this.campaignId = campaignId;
	}
	public String getBatchId() {
		return batchId;
	}
	public void setBatchId(String batchId) {
		this.batchId = batchId;
	}
	public ArrayList<Customer> getCustomers() {
		return customers;
	}
	public void setCustomers(ArrayList<Customer> customers) {
		this.customers = customers;
	}
	
	
	@Override
	public String toString() {
		return "Campaign [campaignId=" + campaignId + ", batchId=" + batchId + ", customers=" + customers + "]";
	}
	
	
}
